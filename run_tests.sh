#!/bin/bash
echo --------------------------------------------------
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'
TEST_BIN_SCANNER=./tests/bin_test_scanner/testbin
TEST_BIN_PARSER=./tests/bin_test_parser/testbin
TMP_OUT_SCANNER=./tests/TMP_OUT
TMP_OUT_PARSER=./tests/TMP_OUT_PARSER


for i in tests/TEST_SCANNER_*/ ;do
  CODE_FILE_SCANNER=${i}TEST_CODE
  OUT_FILE_SCANNER=${i}TEST_OUT
  echo $OUT_FILE_SCANNER
  RETURN_CODE_FILE_SCANNER=${i}TEST_RETURN_CODE

  $TEST_BIN_SCANNER $CODE_FILE_SCANNER > ./$TMP_OUT_SCANNER
  RC_SCANNER=$?

  result_1=$(diff $TMP_OUT_SCANNER $OUT_FILE_SCANNER)

  if [ $? -eq 0 ]
  then
    echo -e "files are the ${GREEN}same${NC}"
  else
    echo -e "files are ${RED}different${NC}"
#    echo $result_1
  fi

  if [ ${RC_SCANNER} -eq $(cat ${RETURN_CODE_FILE_SCANNER}) ]
  then
    echo -e "return code is the ${GREEN}same${NC}"
  else
    echo -e "return code is ${RED}different${NC}"
  fi
#  rm -f $TMP_OUT
done
############################################################
for i in tests/TEST_PARSER_*/ ;do
  CODE_FILE_PARSER=${i}TEST_CODE
  OUT_FILE_PARSER=${i}TEST_OUT
  echo $OUT_FILE_PARSER
  RETURN_CODE_FILE_PARSER=${i}TEST_RETURN_CODE

  $TEST_BIN_PARSER $CODE_FILE_PARSER > ./$TMP_OUT_PARSER
  RC_PARSER=$?

  result_2=$(diff $TMP_OUT_PARSER $OUT_FILE_PARSER)

  if [ $? -eq 0 ]
  then
    echo -e "parser files are the ${GREEN}same${NC}"
  else
    echo -e "parser files are ${RED}different${NC}"
#    echo $result_2
  fi

  if [ ${RC_PARSER} -eq $(cat ${RETURN_CODE_FILE_PARSER}) ]
  then
    echo -e "parser return code is the ${GREEN}same${NC}"
  else
    echo -e "parser return code is ${RED}different${NC}"
  fi
#  rm -f $TMP_OUT
done
