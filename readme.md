# IFJ (Tym 064, varianta b/4/II)

#Implementace interpretu imperativního jazyka IFJ16.

## Commit zmen do repozitare

Pro poradek a vyvarovani se zbytecnym komplikacim dodrzujte alespon nasledujici zakladni pravidla:

	* Do repozitare se commituje pouze funkcni kod. Velmi zjednodusene neco co pujde bez problemu  
	prelozit, pri spusteni nespadne a navic bude fungovat dle specifikace (jiste % neodhalenych  
	chyb pri testovani se muze vyskytnout, ale nemelo by).

	* Commit je nejmensi mnozstvi zmen, ktere spolu navzajem souvisi a splnuji vyse uvedene.  
	Tzn. zadne giganticke commity deseti modulu naraz.
	
	* Pouzivejte srozumitelny format commit messages, ktery presne (bez nahledu na provedene zmeny)  
	vykresli obsah commitu. Idealni tvar je..  
	[Typ (New|Upgrade|Fix)]:[Popis (Jmeno modulu, funkce..)]:[Detailni popis (Pridani parametru bla..)]