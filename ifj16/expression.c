/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of expression solving.
 */

#include "expression.h"

/**
 * @brief Gets operator priority.
 *
 * @param char* string
 *
 * @return int Lower number means higher priority.
 *     -1 In case of undefined operator.
 */
int expression_get_operator_priority(char *string)
{
	if ((strcmp(string, "*") == 0)
		|| (strcmp(string, "/") == 0)
	) {
		return 3;

	} else if ((strcmp(string, "+") == 0)
		|| (strcmp(string, "-") == 0)
	) {
		return 4;

	} else if ((strcmp(string, "<") == 0)
		|| (strcmp(string, ">") == 0)
		|| (strcmp(string, "<=") == 0)
		|| (strcmp(string, ">=") == 0)
	) {
		return 6;

	} else if ((strcmp(string, "==") == 0)
		|| (strcmp(string, "!=") == 0)
	) {
		return 7;
	}

	return -1;
}

/**
 * @brief Converts expression from infix to postfix notation.
 *
 * @param TInterpreterStack* postfix_expression
 * @param TInterpreterStack* infix_expression
 *
 * @return TError ERROR_OK. For other error state see interpreter_init_stack() and interpreter_push_stack() and interpreter_push_inverted_stack().
 */
TError expression_infix_to_postfix(TInterpreterStack *postfix_expression, TInterpreterStack *infix_expression)
{
	TError error_state = ERROR_OK;
	TInterpreterStack *operator_stack = NULL;
	TInterpreterStackItem infix_item, temporary_item;

	error_state = interpreter_init_stack(&operator_stack);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	while (true) {

		infix_item = interpreter_pop_inverted_stack(infix_expression);

		if (infix_item.data_type == INTERPRETER_STACK_DATA_TYPE_EMPTY) {
			break;
		}

		if (infix_item.data_type == INTERPRETER_STACK_DATA_TYPE_OPERATOR) {

			// Read top item
			temporary_item = interpreter_read_stack(operator_stack);

			// If the incoming symbol is a left parenthesis, push it on the stack.
			if (strcmp(infix_item.data.string, "(") == 0) {
				error_state = interpreter_push_stack(operator_stack, infix_item);

				if (error_state != ERROR_OK) {
					interpreter_clear_stack(&operator_stack);
					return error_state;
				}

			// If the stack is empty or contains a left parenthesis on top,
			// push the incoming operator onto the stack.
			} else if (interpreter_is_empty_stack(operator_stack)
				|| (strcmp(temporary_item.data.string, "(") == 0)
			) {
				error_state = interpreter_push_stack(operator_stack, infix_item);

				if (error_state != ERROR_OK) {
					interpreter_clear_stack(&operator_stack);
					return error_state;
				}

			// If the incoming symbol is a right parenthesis, pop the stack
			// and print the operators until you see a left parenthesis.
			// Discard the pair of parentheses.
			} else if (strcmp(infix_item.data.string, ")") == 0) {

				while (true) {

					infix_item = interpreter_pop_stack(operator_stack);

					if (strcmp(infix_item.data.string, "(") == 0) {
						break;
					}

					error_state = interpreter_push_stack(postfix_expression, infix_item);

					if (error_state != ERROR_OK) {
						interpreter_clear_stack(&operator_stack);
						return error_state;
					}
				}

			// If the incoming symbol has higher precedence than the top of the stack,
			// push it on the stack.
			} else if (expression_get_operator_priority(infix_item.data.string)
				< expression_get_operator_priority(temporary_item.data.string)
			) {
				error_state = interpreter_push_stack(operator_stack, infix_item);

				if (error_state != ERROR_OK) {
					interpreter_clear_stack(&operator_stack);
					return error_state;
				}

			// 	If the incoming symbol has equal precedence with the top of the stack,
			// use association. If the association is left to right, pop and print
			// the top of the stack and then push the incoming operator. If the
			// association is right to left, push the incoming operator.
			} else if (expression_get_operator_priority(infix_item.data.string)
				== expression_get_operator_priority(temporary_item.data.string)
			) {
				temporary_item = interpreter_pop_stack(operator_stack);

				error_state = interpreter_push_stack(postfix_expression, temporary_item);

				if (error_state != ERROR_OK) {
					interpreter_clear_stack(&operator_stack);
					return error_state;
				}

				error_state = interpreter_push_stack(operator_stack, infix_item);

				if (error_state != ERROR_OK) {
					interpreter_clear_stack(&operator_stack);
					return error_state;
				}

			// If the incoming symbol has lower precedence than the symbol on the
			// top of the stack, pop the stack and print the top operator. Then test
			// the incoming operator against the new top of stack.
			} else if (expression_get_operator_priority(infix_item.data.string)
				> expression_get_operator_priority(temporary_item.data.string)
			) {
				temporary_item = interpreter_pop_stack(operator_stack);

				error_state = interpreter_push_stack(postfix_expression, temporary_item);

				if (error_state != ERROR_OK) {
					interpreter_clear_stack(&operator_stack);
					return error_state;
				}

				error_state = interpreter_push_inverted_stack(infix_expression, infix_item);

				if (error_state != ERROR_OK) {
					interpreter_clear_stack(&operator_stack);
					return error_state;
				}
			}

		// Print operands as they arrive.
		} else {
			error_state = interpreter_push_stack(postfix_expression, infix_item);

			if (error_state != ERROR_OK) {
				interpreter_clear_stack(&operator_stack);
				return error_state;
			}
		}
	}

	// At the end of the expression, pop and print all operators on the stack.
	// (No parentheses should remain.)
	while (true) {

		temporary_item = interpreter_pop_stack(operator_stack);

		if (temporary_item.data_type == INTERPRETER_STACK_DATA_TYPE_EMPTY) {
			break;
		}

		error_state = interpreter_push_stack(postfix_expression, temporary_item);

		if (error_state != ERROR_OK) {
			interpreter_clear_stack(&operator_stack);
			return error_state;
		}
	}

	interpreter_clear_stack(&operator_stack);

	return error_state;	
}

/**
 * @brief Solves expression.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStack* infix_expression
 * @param tHTable* function_symbol_table
 * @param tHTable* class_symbol_table
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 *     For other error states see interpreter_init_stack() and expression_infix_to_postfix().
 */
TError expression_solve(TInterpreterStackItem *result, TInterpreterStack *infix_expression, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;
	TInterpreterStack *postfix_expression = NULL;
	TInterpreterStack *result_stack = NULL;
	TInterpreterStackItem temporary_result, temporary_item, x, y;

	error_state = interpreter_init_stack(&postfix_expression);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	error_state = expression_infix_to_postfix(postfix_expression, infix_expression);

	if (error_state != ERROR_OK) {
		interpreter_clear_stack(&postfix_expression);
		return error_state;
	}

	error_state = interpreter_init_stack(&result_stack);

	if (error_state != ERROR_OK) {
		interpreter_clear_stack(&postfix_expression);
		return error_state;
	}

	while (true) {

		temporary_item = interpreter_pop_inverted_stack(postfix_expression);

		if (temporary_item.data_type == INTERPRETER_STACK_DATA_TYPE_EMPTY) {
			break;
		}

		// Push operand to the result stack
		if (temporary_item.data_type != INTERPRETER_STACK_DATA_TYPE_OPERATOR) {
			error_state = interpreter_push_stack(result_stack, temporary_item);

			if (error_state != ERROR_OK) {
				interpreter_clear_stack(&postfix_expression);
				interpreter_clear_stack(&result_stack);
				return error_state;
			}

		// Do some operation
		} else {

			temporary_result.data_type = INTERPRETER_STACK_DATA_TYPE_EMPTY;
			y = interpreter_pop_stack(result_stack);
			x = interpreter_pop_stack(result_stack);
			
			// Multiplication
			if (strcmp(temporary_item.data.string, "*") == 0) {
				error_state = expression_multiply(&temporary_result, &x, &y, function_symbol_table, class_symbol_table, symbol_table);

			// Division
			} else if (strcmp(temporary_item.data.string, "/") == 0) {
				error_state = expression_divide(&temporary_result, &x, &y, function_symbol_table, class_symbol_table, symbol_table);
			
			// Addition
			} else if (strcmp(temporary_item.data.string, "+") == 0) {
				error_state = expression_add(&temporary_result, &x, &y, function_symbol_table, class_symbol_table, symbol_table);
			
			// Subtraction	
			} else if (strcmp(temporary_item.data.string, "-") == 0) {
				error_state = expression_subtract(&temporary_result, &x, &y, function_symbol_table, class_symbol_table, symbol_table);
				
			// Is lower
			} else if (strcmp(temporary_item.data.string, "<") == 0) {
				error_state = expression_is_lower(&temporary_result, &x, &y, function_symbol_table, class_symbol_table, symbol_table);
				
			// Is greater
			} else if (strcmp(temporary_item.data.string, ">") == 0) {
				error_state = expression_is_greater(&temporary_result, &x, &y, function_symbol_table, class_symbol_table, symbol_table);
				
			// Is lower or equal
			} else if (strcmp(temporary_item.data.string, "<=") == 0) {
				error_state = expression_is_lower_or_equal(&temporary_result, &x, &y, function_symbol_table, class_symbol_table, symbol_table);
			
			// Is greater or equal	
			} else if (strcmp(temporary_item.data.string, ">=") == 0) {
				error_state = expression_is_greater_or_equal(&temporary_result, &x, &y, function_symbol_table, class_symbol_table, symbol_table);
			
			// Is equal
			} else if (strcmp(temporary_item.data.string, "==") == 0) {
				error_state = expression_is_equal(&temporary_result, &x, &y, function_symbol_table, class_symbol_table, symbol_table);

			// Is not equal
			} else if (strcmp(temporary_item.data.string, "!=") == 0) {
				error_state = expression_is_not_equal(&temporary_result, &x, &y, function_symbol_table, class_symbol_table, symbol_table);

			} else {
				error_state = ERROR_INTERNAL;
			}

			if (error_state != ERROR_OK) {
				interpreter_clear_stack(&postfix_expression);
				interpreter_clear_stack(&result_stack);
				return error_state;
			}

			if (temporary_result.data_type != INTERPRETER_STACK_DATA_TYPE_EMPTY) {
				error_state = interpreter_push_stack(result_stack, temporary_result);

				if (error_state != ERROR_OK) {
					interpreter_clear_stack(&postfix_expression);
					interpreter_clear_stack(&result_stack);
					return error_state;
				}
			}
		}
	}

	*result = interpreter_pop_stack(result_stack);

	interpreter_clear_stack(&postfix_expression);
	interpreter_clear_stack(&result_stack);
	
	return error_state;
}

/**
 * @brief Wrapper for loading indirect value
 *
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* function_symbol_table
 * @param tHTable* class_symbol_table
 * @param tHTable* symbol_table
 *
 * @return TError
 */
TError expression_load_indirect_value(TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;

	if (interpreter_has_data_type_id(x->data_type)) {

		error_state =  interpreter_load_indirect_value(x->data.string, x, function_symbol_table, class_symbol_table, symbol_table);

		if (error_state != ERROR_OK) {
			return error_state;
		}
	}

	if (interpreter_has_data_type_id(y->data_type)) {

		error_state =  interpreter_load_indirect_value(y->data.string, y, function_symbol_table, class_symbol_table, symbol_table);

		if (error_state != ERROR_OK) {
			return error_state;
		}
	}

	return error_state;
}

/**
 * @brief Multiplies x and y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_multiply(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;

	error_state = expression_load_indirect_value(x, y, function_symbol_table, class_symbol_table, symbol_table);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// int * int
	if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_INT;
		result->data.number_integer = x->data.number_integer * y->data.number_integer;
	
	// double * double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;
		result->data.number_double = x->data.number_double * y->data.number_double;
	
	// double * int
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;
		result->data.number_double = x->data.number_double * y->data.number_integer;

	// int * double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;
		result->data.number_double = x->data.number_integer * y->data.number_double;

	} else {
		error_state = ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL;
	}

	return error_state;
}

/**
 * @brief Divides x and y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_divide(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;

	error_state = expression_load_indirect_value(x, y, function_symbol_table, class_symbol_table, symbol_table);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// int / int
	if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_INT;

		if (y->data.number_integer == 0) {
			return ERROR_INTERPRETER_DIVISION_BY_ZERO;
		}

		result->data.number_integer = x->data.number_integer / y->data.number_integer;
	
	// double / double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;

		if (y->data.number_double == 0) {
			return ERROR_INTERPRETER_DIVISION_BY_ZERO;
		}

		result->data.number_double = x->data.number_double / y->data.number_double;
	
	// double / int
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;

		if (y->data.number_integer == 0) {
			return ERROR_INTERPRETER_DIVISION_BY_ZERO;
		}

		result->data.number_double = x->data.number_double / y->data.number_integer;

	// int / double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;

		if (y->data.number_double == 0) {
			return ERROR_INTERPRETER_DIVISION_BY_ZERO;
		}

		result->data.number_double = x->data.number_integer / y->data.number_double;

	} else {
		error_state = ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL;
	}

	return error_state;
}

/**
 * @brief Adds x and y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_add(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;
	char *temporary_string;

	error_state = expression_load_indirect_value(x, y, function_symbol_table, class_symbol_table, symbol_table);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// int + int
	if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_INT;
		result->data.number_integer = x->data.number_integer + y->data.number_integer;
	
	// double + double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;
		result->data.number_double = x->data.number_double + y->data.number_double;
	
	// double + int
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;
		result->data.number_double = x->data.number_double + y->data.number_integer;

	// int + double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;
		result->data.number_double = x->data.number_integer + y->data.number_double;

	// string + string 	
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING;
		error_state = helper_merge_string_string(&(result->data.string), x->data.string, y->data.string);

		if (error_state != ERROR_OK) {
			return error_state;
		}

	// string + double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING;
		error_state = helper_double_to_string(&temporary_string, y->data.number_double);

		if (error_state != ERROR_OK) {
			return error_state;
		}

		error_state = helper_merge_string_string(&(result->data.string), x->data.string, temporary_string);

		if (error_state != ERROR_OK) {
			free(temporary_string);
			return error_state;
		}

		free(temporary_string);
	
	// double + string
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING;
		error_state = helper_double_to_string(&temporary_string, x->data.number_double);

		if (error_state != ERROR_OK) {
			return error_state;
		}

		error_state = helper_merge_string_string(&(result->data.string), temporary_string, y->data.string);

		if (error_state != ERROR_OK) {
			free(temporary_string);
			return error_state;
		}

		free(temporary_string);
	
	// string + int
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING;
		error_state = helper_integer_to_string(&temporary_string, y->data.number_integer);

		if (error_state != ERROR_OK) {
			return error_state;
		}

		error_state = helper_merge_string_string(&(result->data.string), x->data.string, temporary_string);

		if (error_state != ERROR_OK) {
			free(temporary_string);
			return error_state;
		}

		free(temporary_string);
	
	// int + string
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING;
		error_state = helper_integer_to_string(&temporary_string, x->data.number_integer);

		if (error_state != ERROR_OK) {
			return error_state;
		}

		error_state = helper_merge_string_string(&(result->data.string), temporary_string, y->data.string);

		if (error_state != ERROR_OK) {
			free(temporary_string);
			return error_state;
		}

		free(temporary_string);
	
	} else {
		error_state = ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL;
	}

	return error_state;
}

/**
 * @brief Subtracts x and y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_subtract(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;

	error_state = expression_load_indirect_value(x, y, function_symbol_table, class_symbol_table, symbol_table);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// int - int
	if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_INT;
		result->data.number_integer = x->data.number_integer - y->data.number_integer;
	
	// double - double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;
		result->data.number_double = x->data.number_double - y->data.number_double;
	
	// double - int
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;
		result->data.number_double = x->data.number_double - y->data.number_integer;

	// int - double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_DOUBLE;
		result->data.number_double = x->data.number_integer - y->data.number_double;

	} else {
		error_state = ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL;
	}

	return error_state;
}

/**
 * @brief Tells if x is lower than y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_lower(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;

	error_state = expression_load_indirect_value(x, y, function_symbol_table, class_symbol_table, symbol_table);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// int < int
	if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer < y->data.number_integer) ? true : false;
	
	// double < double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double < y->data.number_double) ? true : false;
	
	// double < int
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double < y->data.number_integer) ? true : false;

	// int < double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer < y->data.number_double) ? true : false;

	} else {
		error_state = ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL;
	}

	return error_state;
}

/**
 * @brief Tells if x is greater than y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_greater(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;

	error_state = expression_load_indirect_value(x, y, function_symbol_table, class_symbol_table, symbol_table);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// int > int
	if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer > y->data.number_integer) ? true : false;
	
	// double > double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double > y->data.number_double) ? true : false;
	
	// double > int
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double > y->data.number_integer) ? true : false;

	// int > double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer > y->data.number_double) ? true : false;

	} else {
		error_state = ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL;
	}

	return error_state;
}

/**
 * @brief Tells if x is lower or equal to y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_lower_or_equal(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;

	error_state = expression_load_indirect_value(x, y, function_symbol_table, class_symbol_table, symbol_table);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// int <= int
	if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer <= y->data.number_integer) ? true : false;
	
	// double <= double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double <= y->data.number_double) ? true : false;
	
	// double <= int
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double <= y->data.number_integer) ? true : false;

	// int <= double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer <= y->data.number_double) ? true : false;

	} else {
		error_state = ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL;
	}

	return error_state;
}

/**
 * @brief Tells if x is greater or equal to y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_greater_or_equal(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;

	error_state = expression_load_indirect_value(x, y, function_symbol_table, class_symbol_table, symbol_table);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// int >= int
	if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer >= y->data.number_integer) ? true : false;
	
	// double >= double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double >= y->data.number_double) ? true : false;
	
	// double >= int
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double >= y->data.number_integer) ? true : false;

	// int >= double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer >= y->data.number_double) ? true : false;

	} else {
		error_state = ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL;
	}

	return error_state;
}

/**
 * @brief Tells if x is equal to y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_equal(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;

	error_state = expression_load_indirect_value(x, y, function_symbol_table, class_symbol_table, symbol_table);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// int == int
	if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer == y->data.number_integer) ? true : false;
	
	// double == double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double == y->data.number_double) ? true : false;
	
	// double == int
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double == y->data.number_integer) ? true : false;

	// int == double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer == y->data.number_double) ? true : false;

	} else {
		error_state = ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL;
	}

	return error_state;
}

/**
 * @brief Tells if x is not equal to y.
 *
 * @param TInterpreterStackItem* result
 * @param TInterpreterStackItem* x
 * @param TInterpreterStackItem* y
 * @param tHTable* symbol_table
 *
 * @return TError
 *     ERROR_OK
 */
TError expression_is_not_equal(TInterpreterStackItem *result, TInterpreterStackItem *x, TInterpreterStackItem *y, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;

	error_state = expression_load_indirect_value(x, y, function_symbol_table, class_symbol_table, symbol_table);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// int != int
	if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer != y->data.number_integer) ? true : false;
	
	// double != double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double != y->data.number_double) ? true : false;
	
	// double != int
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_INT)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_double != y->data.number_integer) ? true : false;

	// int != double
	} else if ((x->data_type == INTERPRETER_STACK_DATA_TYPE_INT) && (y->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE)) {
		result->data_type = INTERPRETER_STACK_DATA_TYPE_BOOLEAN;
		result->data.boolean = (x->data.number_integer != y->data.number_double) ? true : false;

	} else {
		error_state = ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL;
	}

	return error_state;
}
