/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of error handling
 */

#ifndef ERROR_H
#define ERROR_H

#include <stdio.h>

/**
 * @brief Return values for error states
 */
typedef enum TError {
	ERROR_OK = 0,
	ERROR_SCANNER_LEXEME = 1,
	ERROR_PARSER_SYNTAX = 2,
	ERROR_SEMANTICS_DEFINITION = 3,
	ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL = 4,
	ERROR_SEMANTICS_OTHER = 6,
	ERROR_INTERPRETER_LOAD_NUMBER = 7,
	ERROR_INTERPRETER_UNINITIALIZED_VARIABLE = 8,
	ERROR_INTERPRETER_DIVISION_BY_ZERO = 9,
	ERROR_INTERPRETER_OTHER = 10,
	ERROR_INTERNAL = 99,
	// Custom error states
	ERROR_HELPER_MATCH = 200,
	ERROR_HELPER_NO_MATCH = 201
} TError;

/**
 * @brief Prints error message according to given state.
 * 
 * @param TError error_state
 * 
 * @return void
 */
void error_print_message(TError error_state);

#endif
