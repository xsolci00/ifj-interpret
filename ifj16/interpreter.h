/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of interpreter
 */

#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "embeded.h"
#include "error.h"
#include "helper.h"
#include "ial.h"
#include "instruction.h"
#include "interpreter_stack.h"
#include "expression.h"

/** @def Name of main class. */
#define MAIN_CLASS_NAME "Main"

/** @def Name of main function. */
#define MAIN_FUNCTION_NAME "run"

/**
 * @brief Interprets instruction list.
 *
 * @param TInstructionList* instruction_list
 * @param tHTable* symbol_table
 *
 * @return TError ERROR_OK.
 */
TError interpreter_run(TInstructionList *instruction_list, tHTable *symbol_table);

/**
 * @brief Checks existence of program enter point.
 *
 * @param tHTable* symbol_table
 *
 * @return TError ERROR_OK. ERROR_SEMANTICS_DEFINITION if enter point is missing.
 */
TError interpreter_check_program_enter_point(tHTable *symbol_table);

#endif
