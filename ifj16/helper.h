/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of general functions
 */

#ifndef HELPER_H
#define HELPER_H

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <regex.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include "error.h"

/** @def int Number of items to be allocated or reallocated in helper_allocate_memmory() */
#define HELPER_ALLOCATE_MEMMORY_SIZE 30

/**
 * @brief Allocates new or extra memmory chunk for the target.
 *
 * @param void** target If *target has NULL value malloc() will be called, realloc() otherwise.
 * @param int target_size Number of elements pointed by target.
 * @param int needed_size Number of elements to be allocated of reallocated. If less than zero default value is HELPER_ALLOCATE_MEMMORY_SIZE.
 * @param int sizeof_type Output of sizeof(element type).
 *
 * @return TError ERROR_INTERNAL if parameter have bad format or memmory cannot be allocated, ERROR_OK otherwise.
 */
TError helper_allocate_memmory(void **target, int target_size, int needed_size, int sizeof_type);

/**
 * @brief Reads string from the source to the first end character or to EOF. Function inserts terminating null byte ('\0').
 *
 * @param char** string
 * @param FILE* source
 * @param char* end_characters List of end characters terminated by null byte ('\0').
 * @param bool include_end_character If true function includes end character into the string.
 *
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError helper_read_string(char **string, FILE *source, char *end_characters, bool include_end_character);

/**
 * @brief Tells if the string matches the pattern.
 *
 * @param char* string
 * @param char* pattern Extended POSIX regular expression.
 *
 * @return TError ERROR_HELPER_MATCH if the string matches the pattern, ERROR_HELPER_NO_MATCH if it does not. ERROR_INTERNAL otherwise.
 */
TError helper_match_pattern(char *string, char *pattern);

/**
 * @brief Converts the string containing integer literal to integer.
 *
 * @param int* number
 * @param char* string
 *
 * @return TError ERROR_OK. ERROR_INTERNAL if converted number is out of range. For other error states see helper_match_pattern().
 */
TError helper_string_to_integer(int *number, char *string);

/**
 * @brief Converts the string containing floating-point literal to double.
 *
 * @param double* number
 * @param char* string
 *
 * @return TError ERROR_OK. ERROR_INTERNAL if converted number is out of range. For other error states see helper_match_pattern().
 */
TError helper_string_to_double(double *number, char *string);

/**
 * @brief Merges two strings.
 *
 * @param char** string
 * @param char* string_1
 * @param char* string_2
 *
 * @return TError
 */
TError helper_merge_string_string(char **string, char *string_1, char * string_2);

/**
 * @brief Converts double to string.
 *
 * @param char** string
 * @param double number
 *
 * @return TError
 */
TError helper_double_to_string(char **string, double number);

/**
 * @brief Converts int to string.
 *
 * @param char** string
 * @param int number
 *
 * @return TError
 */
TError helper_integer_to_string(char **string, int number);

#endif
