/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of main module
 */

#include "ifj16.h"

/**
 * @brief Programs heart.
 *
 * @param int argc
 * @param char** argv
 *
 * @return int For all return codes see TError in error module.
 */
int main(int argc, char *argv[])
{
	TError error_state = ERROR_OK;
	TError error_state_source = ERROR_OK;
	TInputSource source;

	source.input = fopen (argv[1],"r");
	source.line = SCANNER_LINE_COUNTER_DEFAULT;
	
	if (source.input == NULL) {
		error_state_source = ERROR_INTERNAL;
		error_print_message(error_state_source);
		exit(error_state_source);
	}

	error_state = parser(&source);

	if (fclose(source.input) == EOF) {
		error_state_source = ERROR_INTERNAL;
	}

	if (error_state != ERROR_OK) {
		error_print_message(error_state);
		error_print_message(error_state_source);
		exit(error_state);
	}

	error_print_message(error_state_source);
	exit(error_state_source);
}
