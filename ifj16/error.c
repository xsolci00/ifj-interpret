/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of error handling
 */

#include "error.h"

/**
 * @brief Prints error message according to given state.
 * 
 * @param TError error_state
 * 
 * @return void
 */
void error_print_message(TError error_state)
{
	switch (error_state) {
		case ERROR_OK:
			return;
		case ERROR_SCANNER_LEXEME:
			fprintf(stderr, "Lexical error.\n");
			break;
		case ERROR_PARSER_SYNTAX:
			fprintf(stderr, "Syntactic error.\n");
			break;
		case ERROR_SEMANTICS_DEFINITION:
			fprintf(stderr, "Semantic error. Bad definition.\n");
			break;
		case ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL:
			fprintf(stderr, "Semantic error. Incompatible types or bad function call.\n");
			break;
		case ERROR_SEMANTICS_OTHER:
			fprintf(stderr, "Semantic error. Other.\n");
			break;
		case ERROR_INTERPRETER_LOAD_NUMBER:
			fprintf(stderr, "Interpretation error. Number loading.\n");
			break;
		case ERROR_INTERPRETER_UNINITIALIZED_VARIABLE:
			fprintf(stderr, "Interpretation error. Uninitialized variable.\n");
			break;
		case ERROR_INTERPRETER_DIVISION_BY_ZERO:
			fprintf(stderr, "Interpretation error. Division by zero.\n");
			break;
		case ERROR_INTERPRETER_OTHER:
			fprintf(stderr, "Interpretation error. Other.\n");
			break;
		case ERROR_INTERNAL:
			fprintf(stderr, "Internal error.\n");
			break;
		default:
			fprintf(stderr, "Unnamed internal error. \n");
			break;
	}

	return;
}
