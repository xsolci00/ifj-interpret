/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of scanner
 */

#ifndef SCANNER_H
#define SCANNER_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include "error.h"
#include "helper.h"

/** @def string All characters considered as delimiter. */
#define SCANNER_DELIMITER_CHARACTERS " =,<>()+-*/[]{};!"

/** @def int Default state of line counter. */
#define SCANNER_LINE_COUNTER_DEFAULT 1

/** @def int Length of octal escape sequence: '\xxx'. */
#define SCANNER_OCTAL_ESCAPE_SEQUENCE_LENGTH 4

/**
 * @brief Scanner finite state machine states.
 */
typedef enum TScannerState {
	SCANNER_STATE_INIT = 0,

	SCANNER_STATE_COMMENT_BLOCK,
	SCANNER_STATE_COMMENT_BLOCK_END,
	SCANNER_STATE_COMMENT_LINE,
	SCANNER_STATE_EQUALS,
	SCANNER_STATE_GREATER,
	SCANNER_STATE_ID_SIMPLE_OR_KEY_WORD,
	SCANNER_STATE_ID_FULL,
	SCANNER_STATE_LESSER,
	SCANNER_STATE_NOT_EQUALS,
	SCANNER_STATE_NUMBER,
	SCANNER_STATE_NUMBER_EXPONENT,
	SCANNER_STATE_NUMBER_EXPONENT_SIGN,
	SCANNER_STATE_NUMBER_EXPONENT_VALUE,
	SCANNER_STATE_NUMBER_FLOAT,
	SCANNER_STATE_NUMBER_ZERO,
	SCANNER_STATE_SLASH,
	SCANNER_STATE_STRING,
	SCANNER_STATE_STRING_ESCAPE_SEQUENCE
} TScannerState;

/**
 * @brief Token types.
 */
typedef enum TTokenType {
	T_Error = 0,

	T_Asterisk, // *
	T_Left_BBracket, // {
	T_Right_BBracket, // }
	T_Left_Bracket, // (
	T_Right_Bracket, // )
	T_Comma, // ,
	T_Dot, // .
	T_EOF, // End of file
	T_EAL, // End of line
	T_Equals, // =
	T_Greater, // >
	T_Lower, // <
	T_Minus, // -
	T_Plus, // +
	T_Semicolon, // ;
	T_Slash, // /

	T_ID_Simple,
	T_ID_Full,

	T_Boolean,
	T_Break,
	T_Class,
	T_Continue,
	T_Do,
	T_Double,
	T_Else,
	T_False,
	T_For,
	T_If,
	T_Int,
	T_Main,
	T_Return,
	T_Run,
	T_String,
	T_Static,
	T_True,
	T_Void,
	T_While,
	
	T_Number_Double,
	T_Number_Int,

	T_Equals_Equals, // ==
	T_Greater_Equals, // >=
	T_Lower_Equals, // <=
	T_Not_Equals, // !=

	T_Quoted_String
} TTokenType;

/**
 * @brief Token structure.
 */
typedef struct TToken {
	TTokenType type;
	char *data;
	unsigned int data_length_full; // Length including terminating null byte ('\0')
	unsigned int data_length_available; // Available memmory
	unsigned int line;
} TToken;

/**
 * @brief Input source description.
 */
typedef struct TInputSource {
	FILE *input;
	unsigned int line;
} TInputSource;

/**
 * @brief Gets next token from the source.
 *
 * @param TToken* token
 * @param TInputSource* source
 *
 * @return TError
 *     ERROR_OK.
 *     ERROR_SCANNER_LEXEME if bad input is detected.
 *     ERROR_INTERNAL in case of omitted state. 
 *     For error states see helper_allocate_memmory() and scanner_append_data().
 */
TError scanner_get_token(TToken *token, TInputSource *source);

/**
 * @brief Appends the character to the token data.
 *
 * @param TToken* token
 * @param int character
 *
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError scanner_append_data(TToken *token, int character);

/**
 * @brief Initializes the token.
 *
 * @param TToken** token
 *
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError scanner_initialize_token(TToken **token);

/**
 * @brief Initializes token data.
 *
 * @param TToken* token
 *
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError scanner_initialize_token_data(TToken *token);

/**
 * @brief Cleares memmory taken by the token.
 *
 * @param TToken** token
 */
void scanner_clear_token(TToken **token);

/**
 * @brief Copies token data to the data copy.
 *
 * @param TToken* token
 * @param char* data_copy
 * 
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError scanner_copy_token_data(TToken *token, char *data_copy);

/**
 * @brief Tell if given character can be considered as delimiter.
 *
 * @param char c
 *
 * @return bool True if character is delimiter, false otherwise.
 */
bool isdelimiter(char c);

#endif
