/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/*
 * Implementation of print token function
 */

#include "printtoken.h"

/** @var unsigned int For printing readable offset in printtoken_print_all(). */
unsigned int previous_token_line = SCANNER_LINE_COUNTER_DEFAULT;

/**
 * @brief Prints token content.
 *
 * @param TToken* token
 * 
 */
void printtoken_print(TToken *token)
{
    printf(" %i : ", token->line);
    printf("[ ");

    switch (token->type) {

        case T_Error:
            printf("T_Error ");
            break;

        case T_Asterisk:
            printf("T_Asterisk ");
            break;

        case T_Left_BBracket:
            printf("T_Left_BBracket ");
            break;

        case T_Right_BBracket:
            printf("T_Right_BBracket ");
            break;

        case T_Left_Bracket:
            printf("T_Left_Bracket ");
            break;

        case T_Right_Bracket:
            printf("T_Right_Bracket ");
            break;

        case T_Comma:
            printf("T_Comma ");
            break;

        case T_Dot:
            printf("T_Dot ");
            break;

        case T_EOF:
            printf("T_EOF ");
            break;

        case T_EAL:
            printf("T_EAL ");
            break;

        case T_Equals:
            printf("T_Equals ");
            break;

        case T_Greater:
            printf("T_Greater ");
            break;

        case T_Lower:
            printf("T_Lower ");
            break;

        case T_Minus:
            printf("T_Minus ");
            break;

        case T_Plus:
            printf("T_Plus ");
            break;

        case T_Semicolon:
            printf("T_Semicolon ");
            break;

        case T_Slash:
            printf("T_Slash ");
            break;

        case T_ID_Simple:
            printf("T_ID_Simple ");
            break;

        case T_ID_Full:
            printf("T_ID_Full ");
            break;

        case T_Boolean:
            printf("T_Boolean ");
            break;

        case T_Break:
            printf("T_Break ");
            break;

        case T_Class:
            printf("T_Class ");
            break;

        case T_Continue:
            printf("T_Continue ");
            break;

        case T_Do:
            printf("T_Do ");
            break;

        case T_Double:
            printf("T_Double ");
            break;

        case T_Else:
            printf("T_Else ");
            break;

        case T_False:
            printf("T_False ");
            break;

        case T_For:
            printf("T_For ");
            break;

        case T_If:
            printf("T_If ");
            break;

        case T_Int:
            printf("T_Int ");
            break;

        case T_Main:
            printf("T_Main ");
            break;

        case T_Return:
            printf("T_Return ");
            break;

        case T_Run:
            printf("T_Run ");
            break;

        case T_String:
            printf("T_String ");
            break;

        case T_Static:
            printf("T_Static ");
            break;

        case T_True:
            printf("T_True ");
            break;

        case T_Void:
            printf("T_Void ");
            break;

        case T_While:
            printf("T_While ");
            break;

        case T_Number_Double:
            printf("T_Number_Double ");
            break;

        case T_Number_Int:
            printf("T_Number_Int ");
            break;

        case T_Equals_Equals:
            printf("T_Equals_Equals ");
            break;

        case T_Greater_Equals:
            printf("T_Greater_Equals ");
            break;

        case T_Lower_Equals:
            printf("T_Lower_Equals ");
            break;

        case T_Not_Equals:
            printf("T_Not_Equals ");
            break;

        case T_Quoted_String:
            printf("T_Quoted_String ");
            break;

        default:
            printf("!!!Unknown token type!!! ");
            break;
    }

    printf("| \'%s\' ]\n", token->data);

    fflush(NULL);
}

/**
 * @brief Prints all tokens from given source.
 *
 * @param TInputSource* source
 */
void printtoken_print_all(TInputSource *source)
{
    TError error_state = ERROR_OK;
    TToken *token = NULL;

    error_state = scanner_initialize_token(&token);

    if (error_state != ERROR_OK) {
        printf("%s\n", "\n!! [1] Terrible mistake happened: Cannot initialize token. !!\n");
    }

    while ((error_state = scanner_get_token(token, source)) == ERROR_OK) {

        // Print offset for better reading.
        if (previous_token_line < token->line) {
            previous_token_line = token->line;
            printf("\n");
        }
        
        printtoken_print(token);
        
        if (token->type == T_EOF) {
            break;
        }
    }

    scanner_clear_token(&token);
    error_print_message(error_state);
}
