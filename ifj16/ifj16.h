/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of main module
 */

#ifndef IFJ16_H
#define IFJ16_H

#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "parser.h"
#include "scanner.h"

#endif
