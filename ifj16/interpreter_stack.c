/* 
 * Implementation of an imperative language interpreter IFJ16
 *
 * xobrus00: Vojtěch Obrusník
 * xsolci00: Vít Solčík
 * xurban65: Cyril Urban
 * xstrec06: Nikola Strečková
 * xrohel01: Marek Rohel
 */

/**
 * Implementation of interpreter stack
 */

#include "interpreter_stack.h"

/**
 * @brief Initializes interpreter stack.
 *
 * @param TInterpreterStack** stack
 *
 * @return TError ERROR_OK. For other error state see helper_allocate_memmory().
 */
TError interpreter_init_stack(TInterpreterStack **stack)
{
	TError error_state = ERROR_OK;

	*stack = NULL;

	error_state = helper_allocate_memmory(
		(void **) stack,
		0, // Stack has no memmory yet
		1, // Single stack is enough
		sizeof(TInterpreterStack)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	(*stack)->stack = NULL;

	error_state = helper_allocate_memmory(
		(void **) &((*stack)->stack),
		0, // Stack has no memmory yet
		-1, // Use dafault
		sizeof(TInterpreterStackItem)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	(*stack)->size = -1;
	(*stack)->size_inverted = -1;
	(*stack)->size_available = HELPER_ALLOCATE_MEMMORY_SIZE;

	return error_state;
}

/**
 * @brief Cleares interpreter stack.
 *
 * @param TInterpreterStack** stack
 */
void interpreter_clear_stack(TInterpreterStack **stack)
{
	free((*stack)->stack);
	free(*stack);
}

/**
 * @brief Pushes the item to the stack.
 *
 * @param TInterpreterStack* stack
 * @param TInterpreterStackItem item
 *
 * @return TError ERROR_OK. For other error states see helper_allocate_memmory().
 */
TError interpreter_push_stack(TInterpreterStack *stack, TInterpreterStackItem item)
{
	TError error_state = ERROR_OK;

	if (!interpreter_is_full_stack(stack)) {
		stack->size++;
		stack->stack[stack->size] = item;
		return error_state;
	}

	error_state = helper_allocate_memmory(
		(void **) &(stack->stack),
		stack->size_available,
		-1, // Use default
		sizeof(TInterpreterStackItem)
	);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	stack->size_available += HELPER_ALLOCATE_MEMMORY_SIZE;
	stack->size++;
	stack->stack[stack->size] = item;

	return error_state;
}

/**
 * @brief Pushes the item to bottom of the stack.
 *
 * @param TInterpreterStack* stack
 * @param TInterpreterStackItem item
 *
 * @return TError ERROR_OK. ERROR_INTERNAL if stack is empty.
 */
TError interpreter_push_inverted_stack(TInterpreterStack *stack, TInterpreterStackItem item)
{
	TError error_state = ERROR_OK;

	if (interpreter_is_full_inverted_stack(stack)) {
		return ERROR_INTERNAL;
	}

	stack->size_inverted--;
	stack->stack[stack->size_inverted] = item;

	return error_state;
}

/**
 * @brief Gets the item from top of the stack.
 *
 * @param TInterpreterStack* stack
 *
 * @return TInterpreterStackItem In case stack is empty, item.data_type is set to INTERPRETER_STACK_DATA_TYPE_EMPTY.
 */
TInterpreterStackItem interpreter_pop_stack(TInterpreterStack *stack)
{
	TInterpreterStackItem item;

	if (interpreter_is_empty_stack(stack)) {
		item.data_type = INTERPRETER_STACK_DATA_TYPE_EMPTY;
		return item;
	}

	item = stack->stack[stack->size];
	stack->size--;

	return item;
}

/**
 * @brief Gets the item from bottom of the stack.
 *
 * @param TInterpreterStack* stack
 *
 * @return TInterpreterStackItem In case stack is empty, item.data_type is set to INTERPRETER_STACK_DATA_TYPE_EMPTY.
 */
TInterpreterStackItem interpreter_pop_inverted_stack(TInterpreterStack *stack)
{
	TInterpreterStackItem item;

	if (interpreter_is_empty_inverted_stack(stack)) {
		item.data_type = INTERPRETER_STACK_DATA_TYPE_EMPTY;
		return item;
	}

	stack->size_inverted++;
	item = stack->stack[stack->size_inverted];

	return item;
}

/**
 * @brief Reads the item from top of the stack.
 *
 * @param TInterpreterStack* stack
 *
 * @return TInterpreterStackItem In case stack is empty, item.data_type is set to INTERPRETER_STACK_DATA_TYPE_EMPTY.
 */
TInterpreterStackItem interpreter_read_stack(TInterpreterStack *stack)
{
	TInterpreterStackItem item;

	if (interpreter_is_empty_stack(stack)) {
		item.data_type = INTERPRETER_STACK_DATA_TYPE_EMPTY;
		return item;
	}

	return stack->stack[stack->size];
}

/**
 * @brief Tells if stack is empty.
 *
 * @param TInterpreterStack* stack
 *
 * @return bool True if stack is empty, false otherwise.
 */
bool interpreter_is_empty_stack(TInterpreterStack *stack)
{
	return (stack->size < 0) ? true : false;
}

/**
 * @brief Tells if stack is empty.
 *
 * @param TInterpreterStack* stack
 *
 * @return bool True if stack is empty, false otherwise.
 */
bool interpreter_is_empty_inverted_stack(TInterpreterStack *stack)
{
	return ((stack->size_inverted >= stack->size) && !interpreter_is_empty_stack(stack)) ? true : false;
}

/**
 * @brief Tells if stack is full.
 *
 * @param TInterpreterStack* stack
 *
 * @return bool True if stack is full, false otherwise.
 */
bool interpreter_is_full_stack(TInterpreterStack *stack)
{
	return (stack->size >= (stack->size_available - 1)) ? true : false;
}

/**
 * @brief Tells if stack is full.
 *
 * @param TInterpreterStack* stack
 *
 * @return bool True if stack is full, false otherwise.
 */
bool interpreter_is_full_inverted_stack(TInterpreterStack *stack)
{
	return ((stack->size_inverted <= 0) && !interpreter_is_empty_stack(stack)) ? true : false;
}

/**
 * @brief Translates instruction data types to interpreter stack data types.
 *
 * @param TInstructionVariable type
 *
 * @return TInterpreterStackDataType
 */
TInterpreterStackDataType interpreter_map_data_type_from_instruction(TInstructionVariable type)
{
	switch (type) {
		case TYPE_INT:
			return INTERPRETER_STACK_DATA_TYPE_INT;

		case TYPE_DOUBLE:
			return INTERPRETER_STACK_DATA_TYPE_DOUBLE;

		case TYPE_STRING:
			return INTERPRETER_STACK_DATA_TYPE_STRING;

		case TYPE_QUOTED_STRING:
			return INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING;

		case TYPE_ID_SIMPLE:
			return INTERPRETER_STACK_DATA_TYPE_ID_SIMPLE;

		case TYPE_ID_FULL:
			return INTERPRETER_STACK_DATA_TYPE_ID_FULL;

		case TYPE_VOID:
			return INTERPRETER_STACK_DATA_TYPE_VOID;

		case TYPE_UNDEF:
		default:
			return INTERPRETER_STACK_DATA_TYPE_UNDEF;
	}

	return INTERPRETER_STACK_DATA_TYPE_UNDEF;
}

/**
 * @brief Converts instruction to stack item.
 *
 * @param TInterpreterStackItem* item
 * @param TInstruction* instruction
 *
 * @return TError ERROR_OK. For other error states see helper_string_to_integer() and helper_string_to_double().
 */
TError interpreter_convert_instruction_to_stack_item(TInterpreterStackItem *item, TInstruction *instruction)
{
	TError error_state = ERROR_OK;

	item->data_type = interpreter_map_data_type_from_instruction(instruction->var_type);

	if (item->data_type == INTERPRETER_STACK_DATA_TYPE_INT) {
		error_state = helper_string_to_integer(
			&(item->data.number_integer),
			(char *) instruction->address_1
		);

	} else if (item->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) {
		error_state = helper_string_to_double(
			&(item->data.number_double),
			(char *) instruction->address_1
		);

	} else {
		error_state = embeded_substr(
			&(item->data.string),
			(char *) instruction->address_1,
			0,
			strlen((char *) instruction->address_1)
		);
	}

	return error_state;
}

/**
 * @brief Pushes the instruction to the statement stack.
 *
 * @param TInterpreterStack* stack
 * @param TInstruction* instruction
 * @param bool is_operator
 *
 * @return TError ERROR_OK. For other error states see interpreter_convert_instruction_to_stack_item() and interpreter_push_stack().
 */
TError interpreter_push_statement_stack(TInterpreterStack *stack, TInstruction *instruction, bool is_operator)
{
	TError error_state = ERROR_OK;
	TInterpreterStackItem item;

	error_state = interpreter_convert_instruction_to_stack_item(
		&item,
		instruction
	);

	if (error_state != ERROR_OK) {
		return ERROR_INTERNAL;
	}

	if (is_operator) {
		item.data_type = INTERPRETER_STACK_DATA_TYPE_OPERATOR;
	}

	error_state = interpreter_push_stack(stack, item);

	return error_state;
}

/**
 * @brief Prints stack.
 *
 * @param TInterpreterStack* stack
 * @param TInterpreterStack* stack
 */
void interpreter_print_stack(TInterpreterStack *stack, bool is_inverted)
{
	TInterpreterStackItem item;

	if (interpreter_is_empty_stack(stack)) {
		return;
	}

	while (true) {

		item = (is_inverted)
			? interpreter_pop_inverted_stack(stack) : interpreter_pop_stack(stack)
		;

		if (item.data_type == INTERPRETER_STACK_DATA_TYPE_EMPTY) {
			break;
		}

		if (item.data_type == INTERPRETER_STACK_DATA_TYPE_INT) {
			printf("%d ", item.data.number_integer);
		
		} else if (item.data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) {
			printf("%f ", item.data.number_double);

		}  else if (item.data_type == INTERPRETER_STACK_DATA_TYPE_BOOLEAN) {
			printf("%s ", (item.data.boolean) ? "true" : "false");

		} else {
			printf("%s ", item.data.string);
		}
	}

	printf("\n");
}

/**
 * @brief Splits given full id onto namespace and id.
 *
 * @param TFullId* splitted_id
 * @param char* id
 *
 * @return TError
 *     ERROR_OK
 *     ERROR_INTERNAL If given id does not match full id pattern.
 *     For other error states see embeded_substr().
 */
TError interpreter_split_full_id(TFullId *splitted_id, char *id)
{
	TError error_state = ERROR_OK;
	char *temporary_id = id;
	int i;

	error_state = helper_match_pattern(id, INTERPRETER_STACK_FULL_ID_REGEX);

	if (error_state != ERROR_HELPER_MATCH) {
		return ERROR_INTERNAL;
	}

	// Determine namespace
	for (i = 0; *temporary_id != '.'; i++, temporary_id++);

	error_state = embeded_substr(&(splitted_id->namespace), id, 0, i);

	if (error_state != ERROR_OK) {
		return error_state;
	}

	// With + 1 skip dot character ('.')
	error_state = embeded_substr(&(splitted_id->id), id, i + 1, strlen(id) - (i + 1));

	if (error_state != ERROR_OK) {
		return error_state;
	}

	return error_state;
}

/**
 * @brief Cleares content of full id.
 *
 * @param TFullId* id
 */
void interpreter_clear_full_id(TFullId *id)
{
	free(id->namespace);
	free(id->id);

	id->namespace = NULL;
	id->id = NULL;
}

/**
 * @brief Tells if given id is full or not.
 *
 * @param char* id
 * 
 * @return bool True if given id is full id, false otherwise.
 */
bool interpreter_is_full_id(char *id)
{
	TError error_state = ERROR_OK;

	error_state = helper_match_pattern(id, INTERPRETER_STACK_FULL_ID_REGEX);

	if (error_state == ERROR_HELPER_MATCH) {
		return true;
	}

	return false;
}

/**
 * @brief Tells if given data type is id.
 *
 * @param TInterpreterStackDataType type
 * 
 * @return bool True if given data type is id, false otherwise.
 */
bool interpreter_has_data_type_id(TInterpreterStackDataType type)
{
	if ((type == INTERPRETER_STACK_DATA_TYPE_ID_FULL)
		|| (type == INTERPRETER_STACK_DATA_TYPE_ID_SIMPLE)
	) {
		return true;
	}

	return false;
}

/**
 * @brief Translates symbol table data types to interpreter stack data types.
 *
 * @param tDataType type
 *
 * @return TInterpreterStackDataType
 */
TInterpreterStackDataType interpreter_map_data_type_from_symbol_table(tDataType type)
{
	switch (type) {
		case INTEGER:
			return INTERPRETER_STACK_DATA_TYPE_INT;

		case DOUBLE:
			return INTERPRETER_STACK_DATA_TYPE_DOUBLE;

		case STRING:
			return INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING;

		case T_VOID:
			return INTERPRETER_STACK_DATA_TYPE_VOID;

		default:
			return INTERPRETER_STACK_DATA_TYPE_UNDEF;
	}

	return INTERPRETER_STACK_DATA_TYPE_UNDEF;
}

/**
 * @brief Loads value from symbol table.
 *
 * @param char* id
 * @param TInterpreterStackItem* value
 * @param tHTable* function_symbol_table
 * @param tHTable* class_symbol_table
 * @param tHTable* symbol_table
 *
 * @return TError
 */
TError interpreter_load_indirect_value(char *id, TInterpreterStackItem *value, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;
	tHTItem *symbol_table_item;
	TFullId full_id;

	// Full id
	if (interpreter_is_full_id(id)) {

		error_state = interpreter_split_full_id(&full_id, id);

		if (error_state != ERROR_OK) {
			return ERROR_INTERNAL;
		}

		symbol_table_item = htSearch(symbol_table, full_id.namespace);

		if (symbol_table_item == NULL) {
			return ERROR_SEMANTICS_DEFINITION;
		}

		if (symbol_table_item->data_type != HASH_TABLE) {
			return ERROR_INTERNAL;
		}

		symbol_table_item = htSearch((tHTable *) symbol_table_item->data, full_id.id);

		if (symbol_table_item == NULL) {
			return ERROR_SEMANTICS_DEFINITION;
		}

		interpreter_clear_full_id(&full_id);

	// Simple id
	} else {

		symbol_table_item = htSearch(function_symbol_table, id);

		if (symbol_table_item == NULL) {
			
			symbol_table_item = htSearch(class_symbol_table, id);

			if (symbol_table_item == NULL) {
				return ERROR_SEMANTICS_DEFINITION;
			}
		}

	}

	value->data_type = interpreter_map_data_type_from_symbol_table(symbol_table_item->data_type);

	// Int
	if (value->data_type == INTERPRETER_STACK_DATA_TYPE_INT) {

		error_state = helper_string_to_integer(
			&(value->data.number_integer),
			(char *) symbol_table_item->data
		);

		if (error_state != ERROR_OK) {
			return ERROR_INTERNAL;
		}

	// Double
	} else if (value->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) {

		error_state = helper_string_to_double(
			&(value->data.number_double),
			(char *) symbol_table_item->data
		);

		if (error_state != ERROR_OK) {
			return ERROR_INTERNAL;
		}

	} else if (value->data_type == INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING) {

		error_state = embeded_substr(
			(char **) &(value->data.string),
			(char *) symbol_table_item->data,
			0,
			strlen((char *) symbol_table_item->data)
		);

		if (error_state != ERROR_OK) {
			return error_state;
		}
	}

	return error_state;
}

/**
 * @brief Loads value from symbol table.
 *
 * @param char* id
 * @param TInterpreterStackItem* value
 * @param tHTable* function_symbol_table
 * @param tHTable* class_symbol_table
 * @param tHTable* symbol_table
 *
 * @return TError
 */
TError interpreter_set_indirect_value(char *id, TInterpreterStackItem *value, tHTable *function_symbol_table, tHTable *class_symbol_table, tHTable *symbol_table)
{
	TError error_state = ERROR_OK;
	tHTItem *symbol_table_item;
	TFullId full_id;

	// Full id
	if (interpreter_is_full_id(id)) {

		error_state = interpreter_split_full_id(&full_id, id);

		if (error_state != ERROR_OK) {
			return ERROR_INTERNAL;
		}

		symbol_table_item = htSearch(symbol_table, full_id.namespace);

		if (symbol_table_item == NULL) {
			return ERROR_SEMANTICS_DEFINITION;
		}

		if (symbol_table_item->data_type != HASH_TABLE) {
			return ERROR_INTERNAL;
		}

		symbol_table_item = htSearch((tHTable *) symbol_table_item->data, full_id.id);

		if (symbol_table_item == NULL) {
			return ERROR_SEMANTICS_DEFINITION;
		}

		interpreter_clear_full_id(&full_id);

	// Simple id
	} else {

		symbol_table_item = htSearch(function_symbol_table, id);

		if (symbol_table_item == NULL) {
			
			symbol_table_item = htSearch(class_symbol_table, id);

			if (symbol_table_item == NULL) {
				return ERROR_SEMANTICS_DEFINITION;
			}
		}

	}

	// Check types
	if (interpreter_map_data_type_from_symbol_table(symbol_table_item->data_type) != value->data_type) {
		return ERROR_SEMANTICS_TYPE_COMPATIBILITY_AND_FUNCTION_CALL;
	}

	// Free old data if any
	if (symbol_table_item->data != NULL) {
		free(symbol_table_item->data);
	}

	// Set int
	if (value->data_type == INTERPRETER_STACK_DATA_TYPE_INT) {
		error_state = helper_integer_to_string(
			(char **) &(symbol_table_item->data),
			value->data.number_integer
		);

		if (error_state != ERROR_OK) {
			return error_state;
		}

	// Set double
	} else if (value->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) {
		error_state = helper_double_to_string(
			(char **) &(symbol_table_item->data),
			value->data.number_double
		);

		if (error_state != ERROR_OK) {
			return error_state;
		}

	// Set string and others
	} else if (value->data_type == INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING) {
		error_state = embeded_substr(
			(char **) &(symbol_table_item->data),
			value->data.string,
			0,
			strlen(value->data.string)
		);

		if (error_state != ERROR_OK) {
			return error_state;
		}
	}

	return error_state;
}

/**
 * @brief Loads value from symbol table.
 *
 * @param char* id
 * @param TInstructionVariable* type
 * @param tHTable* function_symbol_table
 * @param tHTable* class_symbol_table
 *
 * @return TError
 */
TError interpreter_declare(char *id, TInstructionVariable type, tHTable *function_symbol_table, tHTable *class_symbol_table)
{
	TError error_state = ERROR_OK;
	tHTable *symbol_table = (function_symbol_table == NULL) ? class_symbol_table : function_symbol_table;
	tHTItem *symbol_table_item;
	tDataType data_type = T_VOID;

	// Is it redefinition
	symbol_table_item = htSearch(symbol_table, id);

	if (symbol_table_item != NULL) {
		return ERROR_SEMANTICS_DEFINITION;
	}

	// Determine data type
	if (type == TYPE_VOID) {
		data_type = T_VOID;

	} else if (type == TYPE_STRING) {
		data_type = STRING;

	} else if (type == TYPE_DOUBLE) {
		data_type = DOUBLE;

	} else if (type == TYPE_INT) {
		data_type = INTEGER;
	}

	// Insert into the symbol table
	htInsert(symbol_table, (tKey) id, NULL, NULL, NULL, data_type);

	return error_state;
}

/**
 * @brief Prints value of stack item based on its type.
 *
 * @pram TInterpreterStackItem* value
 */
void interpreter_print_value_based_on_type(TInterpreterStackItem *value)
{
	if (value->data_type == INTERPRETER_STACK_DATA_TYPE_INT) {
		printf("(int): %d\n", value->data.number_integer);
	
	} else if (value->data_type == INTERPRETER_STACK_DATA_TYPE_DOUBLE) {
		printf("(double): %g\n", value->data.number_double);
	
	} else if (value->data_type == INTERPRETER_STACK_DATA_TYPE_QUOTED_STRING) {
		printf("(string): %s\n", value->data.string);
	
	} else if (value->data_type == INTERPRETER_STACK_DATA_TYPE_BOOLEAN) {
		printf("(boolean): %s\n", value->data.boolean ? "true" : "false");
	
	} else {
		printf("%s\n", "Value type is not in {int | double | string | boolean}");
	}
}
